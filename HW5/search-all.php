<!DOCTYPE html>
<html>
    <!-- Web Programming Step by Step, Homework 5 (Kevin Bacon) -->
    <head>
        <title>My Movie Database (MyMDb)</title>
        <meta charset="utf-8" />

        <!-- Links to provided files.  Do not edit or remove these links -->
        <link href="favicon.png" type="image/png" rel="shortcut icon" />
        <script src="provided.js" type="text/javascript"></script>

        <!-- Link to your CSS file that you should edit -->
        <link href="bacon.css" type="text/css" rel="stylesheet" />
    </head>

    <body>

        <div>

            <?php
                $firstname = $_GET["firstname"];
                $lastname = $_GET["lastname"];
                ?>

                <?= $firstname ?> <?= $lastname ?>

        </div>

        <ul>
            <?php

                $server="localhost";
                $user="root";
                $pass="";

                $db = new PDO("mysql:host=$server;dbname=imdb", $user, $pass);
                $rows = $db->query("SELECT name FROM movies m JOIN roles r ON r.movie_id = m.id JOIN actors a ON r.actor_id = a.id WHERE a.first_name= '$firstname' AND a.last_name='$lastname' ");
                foreach ($rows as $row) {
                ?>
                <p>
                  <?= $row["name"]; ?>
                </p>
                <?php
                }
                ?>

        </ul>


    </body>
    </html>




Arduino: 1.8.7 (Windows 8.1), Board: "Arduino/Genuino Uno"

Sketch uses 8790 bytes (27%) of program storage space. Maximum is 32256 bytes.
Global variables use 517 bytes (25%) of dynamic memory, leaving 1531 bytes for local variables. Maximum is 2048 bytes.
avrdude: ser_open(): can't open device "\\.\COM4": The semaphore timeout period has expired.


Problem uploading to board.  See http://www.arduino.cc/en/Guide/Troubleshooting#upload for suggestions.

This report would have more information with
"Show verbose output during compilation"
option enabled in File -> Preferences.