<!-- /* Alegria Haro *** WebDev */ -->
<!DOCTYPE html>
<html>
 <head>
        <title> Rancid Tomatoes </title>
        <link href="rotten copy.gif" rel="icon">
        <link rel="stylesheet" type="text/css" href="movie.css">
        <link rel="icon" type="image/png" href="rotten.gif">
 </head>

  <body>
      <div id="banner">
        <img src="banner.png" alt="Rancid Tomatoes" id="otherbanner">
  		</div>
        <?php
          $movie = $_GET["film"];
          $info = file($movie . "/info.txt");
        ?>
          <div id="filmtitle">
            <h1>
              <?= $info[0] . "(" . trim($info[1]) . ")"?>
            </h1>
          </div>


          <div id="reviews">
      			 <div id="rating">

      				 <?php
      					 $rating = intval($info[2]); //this turns the last line in info.txt into an int for comparison
      					if ($rating >= 60) {
      						$ratingImage = '<img src="freshbig.png" class="rating" alt="Fresh" id="bigrate">';
      					}
                else {
      						$ratingImage = '<img src="rottenbig.png" class="rating" alt="Rotten" id="bigrate">';
      					}
      				?>

      				<?= $ratingImage ?>
      				  <strong>
      					<?= $rating . "%" ?>
      				  </strong>

                <div id="sidebar" class ="column">
                  <img src="<?= $movie ?>/overview.png" alt="Poster" id="movie">
          				 <dl>
          					<?php
          						$overview = file($movie . "/overview.txt"); //this gets overview.txt
          						    foreach ($overview as $line) {
                          ?>
          							<dt><?= substr($line, 0, strpos($line, ':'))?></dt>
          							<dd><?= substr($line, strpos($line, ':') + 1)?></dd>
          					<?php } ?>

          				</dl>
          			</div>
          		</div>
           <div class="overall">
               <div id="reviewcolumn1" class="column">
                 <?php foreach (glob($movie . "/review*.txt") as $review) { //gets review*.txt for every movie
                   $reviewLines = file($review); //gets each line from the movie's review*.txt ?>
                   <div class="reviews">
                     <div class="box">
                       <?php if (strstr($reviewLines[1], 'FRESH')) {
                           $reviewImage = '<img src="fresh.gif" alt="FreshTomato" id="reviewsymbol">';
                             }
                              // if the second line says "ROTTEN" in review.txt then
                            else {
                           $reviewImage = '<img src="rotten.gif" alt="RottenTomato" id="reviewsymbol">';
                           }
                            ?>
                             <?= $reviewImage ?>
                       <q><?= trim($reviewLines[0]) ?></q>
                      </div>
                         <img src="critic.gif" alt="Critic" id="reviewsymbol">
                         <p><?= $reviewLines[2] ?></p>
                         <p><em><?= $reviewLines[3] ?></em></p>
                   </div>
                 <?php
                      }
                 ?>
               </div>
             </div>


             <div id="pages">
               <p>(1-<?= count(glob($movie . "/review*.txt"))?>) of <?= count(glob($movie . "/review*.txt")) ?></p>
             </div>
           </div>

                         <div id="links">
                           <a href="https://validator.w3.org/#validate_by_upload"><img src="w3c-xhtml.png" alt="Valid HTML5"></a> <br>
                           <a href="https://jigsaw.w3.org/css-validator/"><img src="w3c-css.png" alt="Valid CSS"></a>
                         </div>
    </body>
  </html>
